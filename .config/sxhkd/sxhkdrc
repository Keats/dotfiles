#
# bspwm hotkeys
#

super + alt + Escape
	bspc quit

# Close window
super + shift + q
	bspc window -c

# Kill window
super + shift + k
	bspc window -k

# balance tree (in size)
super + b
	bspc desktop -B

# toggle floating or full screen
super + {t,p,s,f}
	bspc window -t {tiled,pseudo_tiled,floating,fullscreen}

# Toggle last focused windows or workspace/desktop back-and-forth
super + {grave,Tab}
	bspc {window,desktop} -f last

# swap desktop with last
super + apostrophe
	bspc window -s last

# Focus the given desktop or move a window to a given desktop
super + {_,shift + }{1-9,0}
	bspc {desktop -f,window -d} ^{1-9,10}

# swap current window with the biggest on screen
super + m
	bspc window -s biggest

# Focus next or prev window
super + {_,shift + }c
	bspc window -f {next,prev}

# Resize window
super + alt + {h,j,k,l}
	bspc window -e {left -10,down +10,up -10,right +10}

# Resize window
super + alt + shift + {h,j,k,l}
	bspc window -e {right -10,up +10,down -10,left +10}

# Switch to previous/next occupied desktop
super + bracket{left,right}
	bspc desktop -f {prev.occupied,next.occupied}

# Presel window for splitting
super + ctrl + {Left,Down,Up,Right}
	bspc window -p {left,down,up,right}

# Circulate leaves of the tree backward or forward
super + {comma,period}
	bspc desktop -C {backward,forward}

# Rotate leaves of tree
super + shift + {comma,period}
	bspc desktop -R {90,270}

# Select windows or move
super + {_,shift + }{Left,Down,Up,Right}
	bspc window -{f,s} {left,down,up,right}

# Cancels a cut
super + ctrl + {_,shift + }space
	bspc {window -p cancel,desktop -c}

# Set the split ratio
super + ctrl + {1-9}
	bspc window -r 0.{1-9}

#
# mouse bindings
#

# Move or resize window
super + button{1-3}
	bspc pointer -g {move,resize_side,resize_corner}

# Needed for ^^^^
super + !button{1-3}
	bspc pointer -t %i %i

super + @button{1-3}
	bspc pointer -u

#
# wm independent hotkeys
#

super + Return
	termite

super + space
	rofi -show run -terminal termite -lines 3 -eh 2 -width 100 -fullscreen -padding 400 -opacity "95" -bw 0 -bc "#282828" -bg "#282828" -fg "#928374" -hlbg "#282828" -hlfg "#ebdbb2"

super + shift + w
	firefox

super + shift + t
	thunar

Print
	scrot -q 100

super + l
	. ~/dotfiles/lock.sh

## Media keys
XF86AudioRaiseVolume
	amixer -q sset Master 5%+

XF86AudioLowerVolume
	amixer -q sset Master 5%-

XF86AudioMute
	amixer -q set Master toggle

# make sxhkd reload its configuration files:
super + Escape
	pkill -USR1 -x sxhkd
